#!/usr/bin/env python

from models import foilviol
import pandas as pd
from sodapy import Socrata
import numpy as np
from mongoengine import *
import pymongo
client = pymongo.MongoClient()
db = client.BuildingData
collection = db.lead_data


# Example authenticated client (needed for non-public datasets):
client = Socrata("data.cityofnewyork.us", "D2AXGY9NcqwJeasQXkaSk5d5w")

results = client.get("b2iz-pps8", limit=2500000, where="novdescription like '%SECTION 27-2056.6%'")
results_df_0 = pd.DataFrame.from_records(results)

results_df = results_df_0.replace(np.nan, '', regex=True)
collection.insert_many(results_df.to_dict('records'))



