#!/usr/bin/env python
from bokeh.io import show, output_notebook, output_file, export_png, export_svgs
from bokeh.models import (
    GeoJSONDataSource,
    HoverTool,
    LinearColorMapper,
    CategoricalColorMapper,
    LogColorMapper,
    ColumnDataSource,
    ColorBar,
    BasicTicker,
    BasicTickFormatter,
    FixedTicker,
    FuncTickFormatter,
    PrintfTickFormatter
)
from bokeh.plotting import figure
from bokeh.palettes import Plasma256, Blues7, Inferno6
from models import lead_data, buildings
import pandas as pd
import numpy as np
from mongoengine import *
import csv
import os
import pymongo

#Create the connection to the mongo instance to pull the lead data
client = pymongo.MongoClient()
db = client.BuildingData
collection = db.lead_data

#Take the geojson data and load it into a Bokeh Datasource made for geo json
with  open(r'NYC_Zip_Code.geojson', 'r') as f:
    geo_source = GeoJSONDataSource(geojson = f.read())
# Load the income data from the csv into a pandas dataframe
income_df = pd.read_csv('income.csv')

# For loading the lead data from mongodb
lng = []
lat = []
df_counts = pd.DataFrame(columns=['count'])
counter = []


# Bring all the data in from Mongodb
viol_count = db.viol_count.find()

#loop through all those results and load them into their respective containers 
c = 0
for j in viol_count:
    longitude = (j['lng'])
    latitude = (j['lat'])
    count = (j['count']) /12
    df_counts.loc[c] = [j['count']]
    counter.append(count)
    lng.append(longitude)
    lat.append(latitude)
    c += 1

# Colors for the data circles on each buildings lead violation counts
colors_all = Plasma256[90:]
colormap = {i: colors_all[i] for i in df_counts['count'].unique()}
colors = [colormap[x] for x in df_counts['count']]

# load all the data into a column datasource from bokeh
source = ColumnDataSource(data=dict(longitude=lng, latitude=lat, color = colors, counts=counter))

#Colors for the zip code income range
color_mapper_income = LinearColorMapper(palette = Blues7)

color_mapper_income_tick = LinearColorMapper(palette = Blues7, low=0, high=60)

color_mapper_lead_tick = LinearColorMapper(palette= Inferno6, low=0, high=60)

income_ticker = FixedTicker(ticks=[3,15, 55])
formatter = FuncTickFormatter(code="""
    data = {3: 'No Income', 15: 'Low Income', 55: 'High Income'}
    return data[tick]
""")

lead_formatter = FuncTickFormatter(code="""
    data = {3: 'Low Violation Count', 15: '', 55: 'High Violation Count'}
    return data[tick]
""")
#what addons we want for the map
TOOLS = "pan,wheel_zoom,box_zoom, reset,hover,save"

# Setting the figure to place the zip code patches and building circles on
p = figure(title="NYC", tools = TOOLS, x_axis_location=None, y_axis_location=None,width=1000,height=700)

p.grid.grid_line_color = None
#The patches drawn using the geo data
p.patches('xs', 'ys', fill_alpha = 0.8, fill_color = {'field': 'OBJECTID', 'transform': color_mapper_income},
line_color='white', line_width=0.7, source = geo_source)
# The circles for the each building that has lead data, color on range of higher counts per building as well as the size is based on the count
p.circle('longitude','latitude', source=source, color='color', size = 'counts', legend = "Building With Lead Violation(s)")
#enable hover
hover = p.select_one(HoverTool)
hover.point_policy = "follow_mouse"
hover.tooltips = [("Zip Code:", "@postalCode"), ("Meidan Income:", "@OBJECTID")]


color_bar_income = ColorBar(color_mapper = color_mapper_income_tick,
                    ticker=income_ticker, formatter = formatter,
                    major_tick_out=0, major_tick_in=0, major_label_text_align='left',
                    major_label_text_font_size='10pt', label_standoff=7, location=(0,450))

color_bar_lead = ColorBar(color_mapper = color_mapper_lead_tick,
                    ticker=income_ticker, formatter = lead_formatter,
                    major_tick_out=0, major_tick_in=0, major_label_text_align='left',
                    major_label_text_font_size='10pt', label_standoff=7, location=(150,450))

p.add_layout(color_bar_income)
p.add_layout(color_bar_lead)


#outputting to an HTML file
output_file("LeadData.html", title="NYC Lead data against income by Zip")

show(p)
