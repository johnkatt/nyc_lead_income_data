#!/usr/bin/env python

from models import buildings
from models import lead_data
import pandas as pd
from sodapy import Socrata
import numpy as np
from mongoengine import *
import pymongo
client = pymongo.MongoClient()
db = client.BuildingData
count_collection = db.viol_count


unique = lead_data.objects.distinct('buildingid')
df_ = pd.DataFrame(columns=['buildindid','count', 'lat','lng'])

for u in unique:
    all = lead_data.objects.filter(buildingid = u)
    count = len(all)
    lat = all[0]['latitude']
    lng = all[0]['longitude']
    df_.loc[u] = [u, count, lat, lng]
    print(count)



count_collection.insert_many(df_.to_dict('records'))